import React, { Component } from "react";

//stateless functional component

const NavBar = ({totalCounters}) => {
  return (
    <nav className="navbar navbar-light bg-light">
      <a className="navbar-brand" href="#">
        Product Cart
        <span className="badge badge-pill badge-secondary ml-1">
          {totalCounters}
        </span>
      </a>
    </nav>
  );
};

export default NavBar;
