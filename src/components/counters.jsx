import React, { Component } from "react";
import Counter from "./counter";

class Counters extends Component {
  
  render() {
    return (
      <div>
        <button
          className="btn btn-primary btn-sm mt-2"
          onClick={this.props.onReset}
        >
          Reset
        </button>

        {this.props.counters.map(counter => (
          <Counter
            key={counter.id}
            selected={true}
            onDelete={this.props.onDelete}
            onIncrement={this.props.onIncrement}
            counter={counter}
          >
            {" "}
          </Counter>
        ))}
      </div>
    );
  }
}

export default Counters;
