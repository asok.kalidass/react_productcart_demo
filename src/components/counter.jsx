import React, { Component } from "react";
class Counter extends Component {
  render() {
    return (
      <div>
        <span className="badge badge-pill badge-secondary .bg-success" style={{backgroundColor: 'DarkCyan'}}>
          {" "}
          {this.props.counter.value}{" "}
        </span>
        <button
          onClick={() => this.props.onIncrement(this.props.counter)}
          className="btn btn-secondary btn-info btn-sm m-2"
        >
          Increment
        </button>
        <button
          className="btn btn-secondary btn-danger btn-sm m-2"
          onClick={() => this.props.onDelete(this.props.counter.id)}
        >
          Delete
        </button>
      </div>
    );
  }
}

export default Counter;
